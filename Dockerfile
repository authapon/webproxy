FROM alpine:latest

COPY caddy /usr/bin/caddy
COPY Caddyfile /etc/caddy/Caddyfile
ADD ./site/ /site/

ENTRYPOINT ["/usr/bin/caddy"]
CMD ["run", "--config", "/etc/caddy/Caddyfile"]
